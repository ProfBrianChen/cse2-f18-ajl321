//Austin Leopold
//[CSE2] Kalafut
//9.25.18
//CrapsIf
//This program uses a nested If statement to run a game of craps outputting the slang terminology. 

import java.util.Random;
import java.util.Scanner;
public class CrapsIf{
  public static void main(String args[]){
    //scanner function for inputing of numbers
   
    Scanner in = new Scanner(System.in);
    int response = 0;
    System.out.println("If you would like to enter numbers for the dice, press 1. Otherwise, press 2 and these numbers will be randomized for you.");
    response = in.nextInt();
    // if user selects "1", enter their own numbers. If user selects "2", randomizes numbers.
    int randomNum1 = 0, randomNum2 = 0;
    //below is the if else statement for entering or randomizing numbers
    if(response == 1) {
      System.out.println("Enter your first number, between 1 and 6.");
      randomNum1 = in.nextInt();
      System.out.println();
      System.out.println("Enter your second number, between 1 and 6.");
      randomNum2 = in.nextInt();
    }
    else
    {
    Random rand = new Random();
    randomNum1 = (rand.nextInt(6)+1);
    randomNum2 = (rand.nextInt(6)+1);
    }
     
    System.out.println("" + randomNum1 + " " + randomNum2);
    //above is the print out for the numbers before the slang
    // below is the switch statement for the slang terms  
    if (randomNum1 == 1) {
      if(randomNum2 == 1)
        System.out.println("Snake Eyes");
      if(randomNum2 == 2)
        System.out.println("Ace Deuce");
      if(randomNum2 == 3)
        System.out.println("Easy Four");
      if(randomNum2 == 4)
        System.out.println("Fever Five");
      if(randomNum2 == 5)
        System.out.println("Easy Six");
      if(randomNum2 == 6)
        System.out.println("Seven Out");
    }
    if (randomNum1 == 2){
      if (randomNum2 == 1)
        System.out.println("Ace Deuce");
      if (randomNum2 == 2)
        System.out.println("Hard Four");
      if (randomNum2 == 3)
        System.out.println("Fever Five");
      if (randomNum2 == 4)
        System.out.println("Easy Six");
      if (randomNum2 == 5)
        System.out.println("Seven Out");
      if (randomNum2 == 6)
        System.out.println("Easy Eight");
    }
    if (randomNum1 == 3){
      if (randomNum2 == 1)
        System.out.println("Easy Four");
      if (randomNum2 == 2)
        System.out.println("Fever Five");
      if (randomNum2 == 3)
        System.out.println("Hard Six");
      if (randomNum2 == 4)
        System.out.println("Seven out");
      if (randomNum2 == 5)
        System.out.println("Easy Eight");
      if (randomNum2 == 6)
        System.out.println("Nine");
     }
    if (randomNum1 == 4){
      if (randomNum2 == 1)
        System.out.println("Fever Five");
      if (randomNum2 == 2)
        System.out.println("Easy Six");
      if (randomNum2 == 3)
        System.out.println("Seven Out");
      if (randomNum2 == 4)
        System.out.println("Hard Eight");
      if (randomNum2 == 5)
        System.out.println("Nine");
      if (randomNum2 == 6)
        System.out.println("Easy Ten");
     }
    if (randomNum1 == 5){
      if (randomNum2 == 1)
        System.out.println("Easy Six");
      if (randomNum2 == 2)
        System.out.println("Seven Out");
      if (randomNum2 == 3)
        System.out.println("Easy Eight");
      if (randomNum2 == 4)
        System.out.println("Nine");
      if (randomNum2 == 5)
        System.out.println("Hard Ten");
      if (randomNum2 == 6)
        System.out.println("Yo-leven");
    }
    if (randomNum1 == 6){
      if (randomNum2 == 1)
        System.out.println("Seven Out");
      if (randomNum2 == 2)
        System.out.println("Easy Eight");
      if (randomNum2 == 3)
        System.out.println("Nine");
      if (randomNum2 == 4)
        System.out.println("Easy Ten");
      if (randomNum2 == 5)
        System.out.println("Yo-leven");
      if (randomNum2 == 6)
        System.out.println("Boxcars");
    }
    
  }
}
