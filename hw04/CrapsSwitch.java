//Austin Leopold
//[CSE2] Kalafut
//9.25.18
//This program uses a nested switch statement to run a game of craps, outputting the slang temrinology.

import java.util.Random;
import java.util.Scanner;
public class CrapsSwitch{
  public static void main(String args[]){
    // below is the statements that decide if the user will enter own numbers or randomize
    Scanner in = new Scanner(System.in);
    int response = 0;
    System.out.println("If you would like to enter numbers for the dice, press 1. Otherwise, press 2 and these numbers will be randomized for you.");
    response = in.nextInt();
    //for user enter own numbers they input "1", if not, user enters "2" for randomization 
    
    int randomNum1 = 0, randomNum2 = 0;
    
    if(response == 1) {
      System.out.println("Enter your first number, between 1 and 6.");
      randomNum1 = in.nextInt();
      System.out.println();
      System.out.println("Enter your second number, between 1 and 6.");
      randomNum2 = in.nextInt();
    }
    //if statement for entering user numbers above
    else if(response ==2)
    {
    Random rand = new Random();
    randomNum1 = (rand.nextInt(6)+1);
    randomNum2 = (rand.nextInt(6)+1);
    }
    //if they select randomize, above else statement runs 
    System.out.println(randomNum1 + " " + randomNum2);
    //above line prints the numbers before the slang
    //below is the nested switch statement for random numbers 
    switch (randomNum1){
      case 1:
        switch(randomNum2){
          case 1:
            System.out.println("Snake eyes");
            break;
          case 2:
            System.out.println("Ace Deuce");
            break;
          case 3:
            System.out.println("Easy Four");
              break;
          case 4:
            System.out.println("Fever Five");
            break;
          case 5:
            System.out.println("Easy Six");
            break;
          case 6:
            System.out.println("Seven out");
            break;
        }
        break;
      case 2:
        switch(randomNum2){
          case 1: 
            System.out.println("Ace Deuce");
            break;
          case 2:
            System.out.println("Hard Four");
            break;
          case 3:
            System.out.println("Fever Five");
            break;
          case 4:
            System.out.println("Easy Six");
            break;
          case 5:
            System.out.println("Seven out");
            break;
          case 6:
            System.out.println("Easy Eight");
            break;
        }
        break;
      case 3:
        switch(randomNum2){
          case 1: 
            System.out.println("Easy Four");
            break;
          case 2:
            System.out.println("Fever Five");
            break;
          case 3:
            System.out.println("Hard Six");
            break;
          case 4:
            System.out.println("Seven out");
            break;
          case 5:
            System.out.println("Easy Eight");
            break;
          case 6:
            System.out.println("Nine");
            break;
        }
        break;
      case 4:
        switch(randomNum2){
            case 1: 
            System.out.println("Fever Five");
            break;
          case 2:
            System.out.println("Hard Six");
            break;
          case 3:
            System.out.println("Seven out");
            break;
          case 4:
            System.out.println("Easy Eight");
            break;
          case 5:
            System.out.println("Nine");
            break;
          case 6:
            System.out.println("Easy Ten");
            break;
        }
        break;
      case 5:
        switch(randomNum2){
           case 1: 
            System.out.println("Easy Six");
            break;
          case 2:
            System.out.println("Seven out");
            break;
          case 3:
            System.out.println("Easy Eight");
            break;
          case 4:
            System.out.println("Nine");
            break;
          case 5:
            System.out.println("Hard Ten");
            break;
          case 6:
            System.out.println("Yo-leven");
            break;
        }
        break;
      case 6:
        switch(randomNum2){
          case 1: 
            System.out.println("Seven Out");
            break;
          case 2:
            System.out.println("Easy Eight");
            break;
          case 3:
            System.out.println("Nine");
            break;
          case 4:
            System.out.println("Easy Ten");
            break;
          case 5:
            System.out.println("Yo-leven");
            break;
          case 6:
            System.out.println("Boxcars");
            break;
        }
    }
   
    }
    }
    

