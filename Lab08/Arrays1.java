//Austin Leopold
//Lab08 Arrays 
//Kalafut [CSE2]
import java.util.Random; 
public class Arrays1{
  public static void main(String [] args){
    Random rand = new Random();
    //The call for a random number within the class
    
    int [] arraynumbers1 = new int [100];
    int [] arraynumbers2 = new int [100];
    int index;
    //Initialize the integers being called
      for (index=0; index<100; index++){
      int rando =rand.nextInt(100);
      arraynumbers1 [index] = rando;
      arraynumbers2 [rando]+=1;
      //For loop that runs the random numbers within the array
      System.out.println(arraynumbers1[index] + ", ");
      }
      //Above, print out statement for the index outputting random numbers 
      for (index = 0; index<100; index++){
        System.out.println(index + " occurs " + arraynumbers2[index] + " times. ");
        //Above, for loop that recognizes that amount of times a number is outputted 
        //Print out statement for the statement "# occurs # times".
        
    }
  }
}