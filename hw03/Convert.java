//Austin Leoold
// [CSE] 02 Convert Class
// 9.18.18
//Kalafut

import java.util.Scanner;

public class Convert{
  
  public static void main(String args []){
    
    Scanner scnr = new Scanner(System.in);
    System.out.println("Enter the affected area in acres: "); //Allow entry of the affected acreage 
    double areaInAcres = scnr.nextDouble();
    System.out.println("Enter the rainfall in the affected area: "); //Allow entry of the rainfall in affected acreage 
    double inchesOfRain = scnr.nextDouble();
    double cubicFeet = (43560 * areaInAcres) / 12.0; //43560=Amt of feet per acre 
    double cubicInches = 1728 * inchesOfRain; //1728 is the Amt of cubic inches in one cubic foot
    double gallons = (cubicFeet * cubicInches) / 231.0; // gives the number of gallons 
    double cubicMiles = gallons * 9.0817e-13;
    
    System.out.print(cubicMiles + " cubic miles"); //Final print out number of cubic miles
    
    
    
    
  }
}     
    
    
      
    