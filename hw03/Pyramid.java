//Austin Leopold
// [CSE2] Kalafut
// Pyramid Class
// 9.18.18

import java.util.Scanner;

public class Pyramid{

public static void main(String args []){
  
   int volPyramid; //initialized variables
   int areaSquare;
   int numHeight;
   int numSide;
  
  Scanner scnr = new Scanner(System.in);
    System.out.println("The square side of the pyramid is (Input length) : ");//Square side of pyramid variable enter
    numSide = scnr.nextInt();
    System.out.println("The height of the pyramid is (input height) : "); //height of pyramid variable enter
    numHeight = scnr.nextInt();  
    
  
  areaSquare = numSide * numSide;// area of bottom square equation
  volPyramid = areaSquare * numHeight / (3); // volume of period calculation equation
  
  System.out.print("The volume inside the pyramid is : " + volPyramid); // final calculation output
  
    }
}

  
    