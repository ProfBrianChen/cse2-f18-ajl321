/* CSE 02-210 WelcomeClass
Austin Leopold
9.3.18
This class prints "Welcome" as well as my Lehigh Network ID as the signature
*/

public class WelcomeClass{
  
  public static void main(String args[]){
    
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-A--J--L--3--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v  ");
    System.out.println("My name is Austin. I'm from Connecticut and I enjoy playing sports");
    
  }
  
}
