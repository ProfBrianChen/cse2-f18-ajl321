// Austin Leopold CSE2 9.5.18 //
// This program is meant to record two types of data. The time elapsed in seconds and the number of rotations of the front wheel in that time 

public class Cyclometer{
  // main method required
  public static void main(String[] args){
    int secsTrip1=480; // number of seconds for trip 1 
    int secsTrip2=3220; // number of seconds for trip 2 
    int countsTrip1=1561; // number of counts for trip 1
    int countsTrip2=9037; // number of counts for trip 2
    double wheelDiameter=27.0, // constant for wheel diameter
    PI=3.14159, // constant for pie 
    feetPerMile=5280, // number of feet per mile 
    inchesPerFoot=12, // number of inches per foot 
    secondsPerMinute=60; // number of seconds per minute 
    double distanceTrip1, distanceTrip2,totalDistance; // output data 
    
    System.out.println("Trip 1 took "+
        (secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
    
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    // above gives distance in inches
    // (for each count, a rotation of the wheel travels the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; 
    //above gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    
    //print out output data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
 //  Trip 1 took 8.0 minutes and had 1561 counts.
// Trip 2 took 53.666666666666664 minutes and had 9037 counts.
// Trip 1 was 2.0897820980113635 miles
// Trip 2 was 12.098245240056817 miles
//The total distance was 14.188027338068181 miles
  }  //end of main method
} //end of class

    