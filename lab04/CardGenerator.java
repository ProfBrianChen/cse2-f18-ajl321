//Austin Leopold
// Lab03 [CSE2] 
//9.19.18
//This program is meant to allow me to practice my card tricks without pysically showing them.

import java.util.Random;

public class CardGenerator{
  public static void main(String args[]){
    //This program will be randomly selecting a card from a deck.
    //the random statement below allows the program to create a random number 
    Random rand = new Random();
    int randomCard = (rand.nextInt(52) + 1);    
    String suit = "";
    String number = "";
  
  //if statement below decides the suit of each card picked  
    
     if (randomCard <= 13){
       suit = "Diamonds";
  }  
    else if (randomCard <= 26){
       suit = "Clubs";
  } 
    else if (randomCard <= 39){
        suit = "Spades";
  } 
    else if (randomCard <= 52){
        suit = "Hearts";
  }

    int cardNumber = 0;
    cardNumber = (randomCard % 13);
    
    // This switch statement creates cases for the card number that will be picked
    switch (cardNumber){
    case 1:
      number = "Ace";
        //System.out.println("You picked the" + number + " of " + )
        break;
    case 2:
      number = "1";
        //System.out.println("You picked the" + number + " of " + suit);
        break;
    case 3:
      number = "2";
        //System.out.println("You picked the" + number + " of " + suit);
        break;
    case 4: 
       number = "3";
        //System.out.println("You picked the" + number + " of " + suit);
        break;
      case 5: 
        number = "4";
        //System.out.println("You picked the" + number + " of " + suit);
        break;
      case 6: 
        number = "5";
        //System.out.println("You picked the" + number + " of " + suit);
          break;
      case 7: 
        number = "6";
        //System.out.println("You picked the" + number + " of " + suit);
          break;
      case 8: 
        number = "7";
        //System.out.println("You picked the" + number + " of " + suit);
          break;
      case 9: 
        number = "8";
        //System.out.println("You picked the" + number + " of " + suit);
        break;
      case 10: 
        number = "9"; 
        //System.out.println("You picked the" + number + " of " + suit);
        break;
      case 11: 
        number = "10"; 
        //System.out.println("You picked the" + number + " of " + suit);
        break;
      case 12: 
        number = "Jack";
        //System.out.println("You picked the" + number + " of " + suit);
        break;
      case 13: 
        number = "Queen";
        //System.out.println("You picked the" + number + " of " + suit);
        break;
      case 14: 
        number = "King";
        //System.out.println("You picked the" + number + " of " + suit); 
        break;
      default: 
        number = "Wildcard";
        break;
        //System.out.println("You picked the" + number + " of " + suit);
        //System.out.println("" + cardNumber);
    }
    //pay no attention to the print statements above 
    // print statement below prints out the selected card
            System.out.println("You picked the " + number + " of " + suit);

}
}

   
    
    