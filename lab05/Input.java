//Austin Leopold
//[CSE2] lab05 Input Variables
//Kalafut
//10.7.18

import java.util.Scanner;
//brings in scanner to program
//Below is opening sequence of a file 
public class Input{
  public static void main(String args[]){
    Scanner scnr = new Scanner(System.in);
    int courseNum = 0;
    String departmentName = "Economics";
    int numOfClasses = 0;
    String time = "12:00";
    String professorName = "Leopold";
    int numOfStudents = 0;
    String junk = "";
    
    //Above are the initialized variables for the input scanner 
    
    System.out.println("Enter the course number: ");
    while (!scnr.hasNextInt()){
      System.out.println("Please enter an integer value for course number: ");
      junk = scnr.nextLine();
    }
    if (scnr.hasNextInt()){
    courseNum = scnr.nextInt();
    junk = scnr.nextLine();
    }
       
    System.out.println("Enter the department name: ");
    departmentName = scnr.nextLine();
    
    
    System.out.println("Enter the number of times the class meets per week: ");
    while (!scnr.hasNextInt()){
      System.out.println("Please enter an integer: ");
      junk = scnr.nextLine();
    }
    if (scnr.hasNextInt()){
    numOfClasses = scnr.nextInt();
    junk = scnr.nextLine();
    }
    System.out.println("Enter the time of the class: ");
    time = scnr.nextLine();
    
    System.out.println("Enter the name of the professor: ");
    professorName = scnr.nextLine();
    
    System.out.println("Enter the number of students in the course: ");
    while (!scnr.hasNextInt()){
      System.out.println("Please enter an integer: ");
      junk = scnr.nextLine();
    }
    if (scnr.hasNextInt()){
      numOfStudents = scnr.nextInt();
      junk = scnr.nextLine();
    }
    //The above are While loops that ask the user to input values and if 
    
    System.out.println("Course number: " + courseNum);
    System.out.println("Department name: " + departmentName);
    System.out.println("Number of times the class meets per week: " + numOfClasses);
    System.out.println("Time of class: " + time);
    System.out.println("Name of professor: " + professorName);
    System.out.println("Number of students in class: " + numOfStudents);
        // Above are the print output statements for this lab 
  }
}


