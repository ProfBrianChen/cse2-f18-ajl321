public class Arithmetic{

 public static void main(String args[]){
   
   
//Number of pairs of pants 
int numPants = 3;
//Cost per pair of pants 
double pantsPrice = 34.98;

//Number of sweatshirts 
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts 
int numBelts = 1;
//cost per belt 
double beltCost = 33.99;

//the tax rate 
double paSalesTax = 0.06;

//total cost of pants 
double totalCostPants;
//total cost of shirts
double totalCostShirts;
//total cost of belts
double totalCostBelts;
//total sales tax on pants
double SalesTaxOnPants;
//total sales tax on shirts
double SalesTaxOnShirts;
//total sales tax on belts
double SalesTaxOnBelts;
//total cost of entire purchase before tax
double totalCostOfEntirePurchaseBeforeTax;
//total sales tax
double totalSalesTax;
//total cost of entire purchase after tax
double totalCostAfterTax;

//equation for total cost of pants
totalCostPants = numPants * pantsPrice;
//equation for total cost of shirts
totalCostShirts = numShirts * shirtPrice;
//equation for the total cost of belts
totalCostBelts = numBelts * beltCost;
//equation for the sales tax on pants 
SalesTaxOnPants = (int) (totalCostPants * paSalesTax * 100) / 100.0;
//equation for the sales tax on shirts
SalesTaxOnShirts = (int) (totalCostShirts * paSalesTax * 100) / 100.0;
//equation for the sales tax on belts
SalesTaxOnBelts = (int) (totalCostBelts * (paSalesTax * 100)) / 100.0;
//equation for total cost of purchases (before tax)
totalCostOfEntirePurchaseBeforeTax = totalCostPants + totalCostShirts + totalCostBelts;
//equation for the total sales tax on the purchase
totalSalesTax = (int) ((totalCostOfEntirePurchaseBeforeTax * paSalesTax ) * 100) / 100.0;
//equation for total paid for this transaction including sales tax
totalCostAfterTax = (int) ((totalCostOfEntirePurchaseBeforeTax + totalSalesTax) * 100) / 100.0; 


System.out.println("Total cost of pants: " + " $ " + totalCostPants);
System.out.println("Total cost of shirts: " + " $ " + totalCostShirts);
System.out.println("Total cost of belts: " + " $ " + totalCostBelts);
System.out.println("Sales tax on pants: " + " $ " + SalesTaxOnPants);
System.out.println("Sales tax on shirts: " + " $ " + SalesTaxOnShirts);
System.out.println("Sales tax on belts: " + " $ " + SalesTaxOnBelts);
System.out.println("Total cost of entire purchase (Before tax): " + " $ " + totalCostOfEntirePurchaseBeforeTax);
System.out.println("Total sales tax on entire purchase: " + " $ " + totalSalesTax);
System.out.println("Total cost of purchase after tax: " + " $ " + totalCostAfterTax);
//the lines above will print out the outputs necessary 

 }
  
}

